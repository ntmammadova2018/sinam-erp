﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Oracle.ManagedDataAccess.Client;

public partial class EditPage : System.Web.UI.Page
{
    DatabaseConnection conn;
    protected void Page_Load(object sender, EventArgs e)
    {        
            conn = new DatabaseConnection();
            if (!IsPostBack)
            {
                fillFields();
            }    
    }
   

    protected void fillFields()
    {
        try
        {
            int reg_id = (int)Session["region_id"];
            string commandStr = "SELECT r.REGION_ID, r.REGION_NAME, c.COUNTRY_NAME, r.SORT_CRITERIA FROM HR.REGIONS_DICT r INNER JOIN HR.COUNTRIES_DICT c ON r.COUNTRY_ID = c.COUNTRY_ID WHERE REGION_ID =" + reg_id;
            OracleCommand command = new OracleCommand(commandStr, conn.getConnection());
            command.CommandType = CommandType.Text;
            command.ExecuteNonQuery();

            OracleDataReader reader = command.ExecuteReader();
            reader.Read();

            txtboxName.Text = reader["REGION_NAME"].ToString();
            txtboxCountry.Text = reader["COUNTRY_NAME"].ToString();
            txtboxSort.Text = reader["SORT_CRITERIA"].ToString();

        }
        catch(Exception ex)
        {
            Response.Write(ex);
        }
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Region.aspx");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        CRUD crud = new CRUD();
        int reg_id = (int) Session["region_id"];        
        string name = txtboxName.Text;
        int sort_criteria = 0;
        try
        {
            Convert.ToInt32(txtboxSort.Text);
            sort_criteria = Convert.ToInt32(txtboxSort.Text);
        }
        catch (Exception ex)
        {

        }
        

        int sucess = crud.Upd_Regions(reg_id, name, sort_criteria, conn.getConnection());
        if (sucess == 1)
        {
            Response.Redirect("Region.aspx");

        }
        else
        {

        }
    }
}