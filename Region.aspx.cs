﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using WebSite1;


public partial class About : Page
{
    DatabaseConnection conn;
    protected void Page_Load(object sender, EventArgs e)
    {
        conn = new DatabaseConnection();
        if(!IsPostBack)
            dropdownLoad();
        Repeater();
    }    
    
    protected void dropdownLoad()
    {
        try
        {
            if (conn.getConnection().State == ConnectionState.Closed)
                conn.getConnection().Open();

            string dropcmdstring = "SELECT COUNTRY_ID, COUNTRY_NAME FROM HR.COUNTRIES_DICT";
            DataTable table = new DataTable();
            OracleDataAdapter adapter = new OracleDataAdapter(dropcmdstring, conn.getConnection());
            adapter.Fill(table);

            dropListCountry.DataTextField = "COUNTRY_NAME";
            dropListCountry.DataValueField = "COUNTRY_ID";

            dropListCountry.DataSource = table;
            dropListCountry.DataBind();

            dropListCountry.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
        }

    }

    protected void asplog(object s)
    {
        Response.Write("<script language=javascript>"+
                    "console.log('" +s+ "');"+
                    "</script>");
    }
    
    private void Repeater()
    {       
        OracleCommand command = new OracleCommand();
        
        try
        {
            if (conn.getConnection().State == ConnectionState.Closed)
                conn.getConnection().Open();

            command = conn.getConnection().CreateCommand();
            int country_id = Convert.ToInt32(dropListCountry.SelectedItem.Value);
           // command.CommandText = "SELECT HR.SLCT_COUNTRIES("+country_id+") AS NAME FROM DUAL";
           // command.CommandType = CommandType.Text;

           /* OracleParameter param_country_id = new OracleParameter();
            param_country_id.OracleDbType = OracleDbType.Decimal;
            param_country_id.Value = dropListCountry.SelectedItem.Value;
            command.Parameters.Add(param_country_id);         */   

            fillRptr(country_id);
            
        }
            catch (Exception ex)
            {
                Console.Write(ex);
            }

            finally
            {
                conn.getConnection().Close();
                command.Dispose();
            }
        }
    
    private void fillRptr(int i_country_id)
    {
        DataTable dt = new DataTable();
        OracleDataAdapter adapter = null;     
          
        string commandStr = "SELECT REGION_ID, REGION_NAME, LAST_UPDATE_DATE FROM HR.REGIONS_DICT WHERE Deleted = 0 AND COUNTRY_ID =" + i_country_id;
        OracleCommand command2 = new OracleCommand(commandStr, conn.getConnection());
        command2.CommandType = CommandType.Text;

        adapter = new OracleDataAdapter(command2);

        OracleCommandBuilder builder = new OracleCommandBuilder(adapter);
        adapter.Fill(dt);

        rptRegions.DataSource = dt;
        rptRegions.DataBind();
                           
        //gridPersons.DataSource = ds.Tables[0];        
        //gridPersons.DataBind();
                
        adapter = null;
    }

    protected void dropListCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        Repeater();
    }    

    protected void rptregions1(object source, RepeaterCommandEventArgs e)
    {
        CRUD crud = new CRUD();
        int reg_id = 0;
        if (e.CommandName != "Insert")
            reg_id = Convert.ToInt32(e.CommandArgument);

        if (conn.getConnection().State == ConnectionState.Closed)
            conn.getConnection().Open();        

        if (e.CommandName == "Delete")
        {            
            int sucess = crud.Del_Regions(reg_id, conn.getConnection());
            if (sucess == 1)
            {
                int country_id = Convert.ToInt32(dropListCountry.SelectedItem.Value);
                fillRptr(country_id);
                
            }
            else{
                
            }
        }

        if (e.CommandName == "Update")
        {
            Session["region_id"] = reg_id;
            Response.Redirect("EditPage.aspx");
        }


        if (e.CommandName == "Insert")
        {
            Session["region_id"] = reg_id;
            Response.Redirect("InsertPage.aspx");
        }

    }
    
}