﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeFile="InsertPage.aspx.cs" Inherits="InsertPage" %>

  
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />

    <h1 style="color:#006dcc">Insert Region</h1>
    <div class="padding-min">

        <div class="padding-bottom">
            <label>Country name</label>
            <asp:DropDownList runat="server" AutoPostBack="True" ID="dropListCountry" CssClass="dropdown-boot" ></asp:DropDownList>
        </div>

        <label>Region name</label>
              <asp:TextBox runat="server" ID="txtboxName" class="form-control" Width="20%"  style="display:inline"  MaxLength="25"></asp:TextBox>   
        
        <label>Sort Criteria</label>
            <asp:TextBox runat="server" ID="txtboxSort" class="form-control" Width="20%"  style="display:inline" TextMode="Number" ToolTip="Can be left empty" ValidateRequestMode="Disabled"></asp:TextBox>                    
        
        <div class="btn-group-spc">
            <asp:Button runat="server" ID="btnSave" Class="btn btn-primary" Text="Save" OnClick="btnSave_Click"/>
            <asp:Button runat="server" ID="btnCancel" Class="btn btn-default" Text="Cancel" OnClick="btnCancel_Click"/>
        </div>
    </div>

    </asp:Content>
