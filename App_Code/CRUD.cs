﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.ManagedDataAccess.Client;

public class CRUD
{
    public CRUD()
    {        
    }


    public int Del_Regions(int id, OracleConnection conn) {
        int success = 0;
        try {  

            OracleCommand delcmd = new OracleCommand("HR.DEL_Regions", conn);
            delcmd.CommandType = CommandType.StoredProcedure;

            delcmd.Parameters.Add("@I_ID", id);
            
            delcmd.ExecuteReader();

            success = 1;       
        }
         catch (Exception ex)
        {
            success = 0;
            Console.Write(ex);
        }

        return success;     
    }

    public int Upd_Regions(int id, string name, int sort_criteria, OracleConnection conn)
    {
        int success = 0;
        try
        {
            OracleCommand updcmd = new OracleCommand("HR.UPD_Regions", conn);
            updcmd.CommandType = CommandType.StoredProcedure;

            updcmd.Parameters.Add("@I_ID", id);
            updcmd.Parameters.Add("@I_Name", name);
            updcmd.Parameters.Add("@I_Sort_Criteria",sort_criteria);

            updcmd.ExecuteNonQuery();

            success = 1;
        }
        catch (Exception ex)
        {
            success = 0;
            Console.Write(ex);
        }

        return success;
    }

    public int Ins_Regions(int id, string name, int sort_criteria, OracleConnection conn)
    {
        int success = 0;
        try
        {
            OracleCommand updcmd = new OracleCommand("HR.INS_Regions", conn);
            updcmd.CommandType = CommandType.StoredProcedure;

            updcmd.Parameters.Add("@I_Country_ID", id);
            updcmd.Parameters.Add("@I_Name", name);
            updcmd.Parameters.Add("@I_Sort_Criteria", sort_criteria);

            updcmd.ExecuteNonQuery();

            success = 1;
        }
        catch (Exception ex)
        {
            success = 0;
            Console.Write(ex);
        }

        return success;
    }

}