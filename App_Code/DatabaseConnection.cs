﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;

/// <summary>
/// Summary description for DatabaseConnection
/// </summary>
public class DatabaseConnection
{
    public DatabaseConnection()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    static OracleConnection conn = new OracleConnection();
    public OracleConnection getConnection()
    {
        if (conn.ConnectionString.Equals("") || conn.ConnectionString == null)
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["ORCL"].ConnectionString;

        return conn;
    }
}