﻿<%@ Page Title="Region" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Region.aspx.cs" Inherits="About" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />

<div class="padding-min">
<asp:Label runat="server" ID="lblTest" Text="Choose a country" />
<asp:DropDownList runat="server" AutoPostBack="True" ID="dropListCountry" OnSelectedIndexChanged="dropListCountry_SelectedIndexChanged" CssClass="dropdown-boot" ></asp:DropDownList>
    <br /> <br />

<asp:Repeater runat="server" ID="rptRegions" OnItemCommand="rptregions1">
    <HeaderTemplate >
        <table>
            <thead>
                <tr> <th>Region Name</th> 
                     <th>Last Update Date</th>
                </tr>
            </thead>
    </HeaderTemplate>   

    <ItemTemplate>            
            <tr>
                <td><%# Eval("REGION_NAME") %> </td>
                <td><%# Eval("LAST_UPDATE_DATE") %> </td>
                 <td>
                    <asp:LinkButton runat="server" ID="lkbupdate" CommandName="Update" CommandArgument='<%# Eval("REGION_ID")%>' >
                       <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> 
                    </asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton runat="server" ID="lkbdelete" CommandName="Delete" CommandArgument='<%# Eval("REGION_ID")%>' >
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </asp:LinkButton>
                </td>               
            </tr>
    </ItemTemplate>  

    <FooterTemplate>      
        </table>          
              <asp:LinkButton runat="server" ID="lkbinsert" CommandName="Insert" CssClass="btn btn-success" >
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </asp:LinkButton>
    </FooterTemplate>
</asp:Repeater> 
    </div>

</asp:Content>
