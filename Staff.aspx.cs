﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Staff : System.Web.UI.Page
{
    DatabaseConnection conn;
    protected void Page_Load(object sender, EventArgs e)
    {
        conn = new DatabaseConnection();
        if (!IsPostBack)
            Repeater();
    }

    private void Repeater()
    {
        OracleCommand command = new OracleCommand();

        try
        {
            if (conn.getConnection().State == ConnectionState.Closed)
                conn.getConnection().Open();

            command = conn.getConnection().CreateCommand();
            int plant_id = Convert.ToInt32(Request.QueryString["value"]);                

            fillRptr(plant_id);

        }
        catch (Exception ex)
        {
            Console.Write(ex);
        }

        finally
        {
            conn.getConnection().Close();
            command.Dispose();
        }
    }

    private void fillRptr(int i_plant_id)
    {
        DataTable dt = new DataTable();
        OracleDataAdapter adapter = null;

        string commandStr = "SELECT  s.ID, po.NAME, s.QUANTITY FROM HR.STAFF_TABLE s " +                            
                            "INNER JOIN HR.POSITIONS po " +
                            "ON s.POSITION_ID = po.ID " +
                            "WHERE s.Deleted = 0 AND s.PLANT_ID = " + i_plant_id;
        OracleCommand command2 = new OracleCommand(commandStr, conn.getConnection());
        command2.CommandType = CommandType.Text;

        adapter = new OracleDataAdapter(command2);

        OracleCommandBuilder builder = new OracleCommandBuilder(adapter);
        adapter.Fill(dt);

        rptStaff.DataSource = dt;
        rptStaff.DataBind();

        adapter = null;
    }

    protected void dropListPlant_SelectedIndexChanged(object sender, EventArgs e)
    {
        Repeater();
    }

    protected void rptStaff_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
}