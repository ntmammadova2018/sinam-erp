﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeFile="Staff.aspx.cs" Inherits="Staff" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />

<div class="padding-min">

<asp:Repeater runat="server" ID="rptStaff" OnItemCommand="rptStaff_ItemCommand">
    <HeaderTemplate >
        <table>
            <thead>
                <tr> <th>Position</th> 
                     <th>Quantity</th>
                </tr>
            </thead>
    </HeaderTemplate>   

    <ItemTemplate>            
            <tr>
                <td><%# Eval("NAME") %> </td>
                <td><%# Eval("QUANTITY") %> </td>
                 <td>
                    <asp:LinkButton runat="server" ID="lkbupdate" CommandName="Update" CommandArgument='<%# Eval("ID")%>' >
                       <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> 
                    </asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton runat="server" ID="lkbdelete" CommandName="Delete" CommandArgument='<%# Eval("ID")%>' >
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </asp:LinkButton>
                </td>               
            </tr>
    </ItemTemplate>  

    <FooterTemplate>      
        </table>          
              <asp:LinkButton runat="server" ID="lkbinsert" CommandName="Insert" CssClass="btn btn-success" >
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </asp:LinkButton>
    </FooterTemplate>
</asp:Repeater> 
    </div>

</asp:Content>