﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InsertPage : System.Web.UI.Page
{
    DatabaseConnection conn;
    protected void Page_Load(object sender, EventArgs e)
    {
        conn = new DatabaseConnection();
        if (!IsPostBack)
        {
            dropdownLoad();
        }
    }

    private void dropdownLoad()
    {
        try
        {
            if (conn.getConnection().State == ConnectionState.Closed)
                conn.getConnection().Open();

            string dropcmdstring = "SELECT COUNTRY_ID, COUNTRY_NAME FROM HR.COUNTRIES_DICT";
            DataTable table = new DataTable();
            OracleDataAdapter adapter = new OracleDataAdapter(dropcmdstring, conn.getConnection());
            adapter.Fill(table);

            dropListCountry.DataTextField = "COUNTRY_NAME";
            dropListCountry.DataValueField = "COUNTRY_ID";

            dropListCountry.DataSource = table;
            dropListCountry.DataBind();

            dropListCountry.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
        }
        finally
        {
            conn.getConnection().Close();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        CRUD crud = new CRUD();
        int country_id = Convert.ToInt32(dropListCountry.SelectedItem.Value);
        string name = txtboxName.Text;
        int sort_criteria = 0;
        try
        {
            Convert.ToInt32(txtboxSort.Text);
            sort_criteria = Convert.ToInt32(txtboxSort.Text);
        }
        catch (Exception ex)
        {

        }

        if (conn.getConnection().State == ConnectionState.Closed)
            conn.getConnection().Open();

        int sucess = crud.Ins_Regions(country_id, name, sort_criteria, conn.getConnection());
        if (sucess == 1)
        {
            Response.Redirect("Region.aspx");

        }
        else
        {

        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Region.aspx");
    }

}